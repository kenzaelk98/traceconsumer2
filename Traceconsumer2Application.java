package es.upm.dit.apsv.traceconsumer2;

import java.util.function.Consumer;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;


import es.upm.dit.apsv.traceconsumer2.Repository.TraceConsumer2Repository;
import es.upm.dit.apsv.traceconsumer2.model.TraceConsumer2;


@SpringBootApplication
public class Traceconsumer2Application {

	public static final Logger log = LoggerFactory.getLogger(Traceconsumer2Application.class);

	@Autowired
	private TraceConsumer2Repository tracerepository;


	public static void main(String[] args) {
		SpringApplication.run(Traceconsumer2Application.class, args);
	}

	@Bean("consumer")
	public Consumer<TraceConsumer2> checkTrace() {
			return t -> {
					t.setTraceId(t.getTruck() + t.getLastSeen());
					tracerepository.save(t);
		
			};
	}
}

